Clarin dspace
=========

A role for setting up clarino dspace repo, based on [clarin-dspace](https://github.com/ufal/clarin-dspace)

The role is made for automating the CLARINO fork, and only works on centos8. All default values are set for clarino, and there exists some hardcoded shortcuts.

Requirements
------------
This role has several requirement. It requires [centos8](https://github.com/ufal/clarin-dspace) as OS, a [role for httpd](https://git.app.uib.no/uib-ub/roller-ansible/centos8) as web-server, [Shibboleth](https://git.app.uib.no/uib-ub/roller-ansible/shibboleth) for AAI and [tomcat](https://git.app.uib.no/uib-ub/roller-ansible/tomcat-centos8) to deploy. 

The following packages are also used by the role.

```
["ant","maven","make","bzip2","git","python2", "java-11-openjdk-devel","unzip","wget"]
```

Additionaly it requires a dspace user, and the dspace user having git access to git repos by private key.
Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
